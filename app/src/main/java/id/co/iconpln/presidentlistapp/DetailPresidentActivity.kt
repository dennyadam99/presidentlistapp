package id.co.iconpln.presidentlistapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_detail_president.*

class DetailPresidentActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        const val EXTRA_NAME = "extra_name"
        const val EXTRA_DESC = "extra_desc"
        const val EXTRA_IMAGE_URL = "extra_image_url"

        const val EXTRA_PRESIDENT = "extra_president"

    }

    private lateinit var president: President

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_president)
        setupActionBar()
        initIntentExtras()
        displayPresidentDetail()
        setOnClickListener()
    }

    private fun setOnClickListener() {
        btnShareDetail.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnShareDetail -> {
                var shareTexts = this.president.name + "\n\n"
                shareTexts += this.president.desc + "\n\n"
                shareTexts += this.president.photo + "\n"

                val sharedText = this.president.name
                val shareTextIntent = Intent(Intent.ACTION_SEND)
                shareTextIntent.putExtra(Intent.EXTRA_TEXT, sharedText)
                shareTextIntent.type = "TEXT/PLAIN"
                shareTextIntent.putExtra(Intent.EXTRA_SUBJECT, "PRESIDENT")
                shareTextIntent.putExtra(Intent.EXTRA_TEXT, shareTexts)
                if (shareTextIntent.resolveActivity(packageManager) != null) {
                    startActivity(Intent.createChooser(shareTextIntent, "SHARE"))
                }

            }
        }
    }

    private fun initIntentExtras() {
        president = intent.getParcelableExtra(EXTRA_PRESIDENT)

    }

    private fun setupActionBar() {
        supportActionBar?.title = "Detail President"
        supportActionBar?.setDisplayHomeAsUpEnabled((true))
    }

    private fun displayPresidentDetail() {
        tvPresidentDetailName.text = president.name
        tvPresidentDetailDescription.text = president.desc
        Glide.with(this)
            .load(president.photo)
            .apply(
                RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.ic_launcher_foreground)
                    .error(R.drawable.ic_launcher_foreground)
            )

            .into(ivPresidentDetailImage)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> {
                false
            }
        }
    }
}

