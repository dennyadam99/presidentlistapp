package id.co.iconpln.presidentlistapp

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class President (
    var name: String="",
    var desc: String="",
    var photo: String=""
):Parcelable