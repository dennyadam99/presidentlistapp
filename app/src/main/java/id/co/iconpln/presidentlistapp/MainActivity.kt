package id.co.iconpln.presidentlistapp

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val listPresident: ListView
        get() = lv_list_president

    private var list: ArrayList<President> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupActionBar()
        loadListArrayAdapter()
        loadListBaseAdapter(this)
        setListItemClickListener(listPresident)
    }

    private fun setupActionBar() {
        supportActionBar?.title = "President List"
        supportActionBar?.setDisplayHomeAsUpEnabled((true))

    }

    private fun setListItemClickListener(listView: ListView) {
        listView.onItemClickListener =
            AdapterView.OnItemClickListener { adapterView, _, index, _ ->
                val president = adapterView?.getItemAtPosition(index) as President
                Toast.makeText(this@MainActivity, "${president.name}", Toast.LENGTH_SHORT).show()
                showDetailPresident(list[index])
            }
    }

    private fun showDetailPresident(president: President) {
        val detailPresidentIntent = Intent(this, DetailPresidentActivity::class.java)
        detailPresidentIntent.putExtra(DetailPresidentActivity.EXTRA_PRESIDENT, president)
        startActivity(detailPresidentIntent)
    }
    fun loadListArrayAdapter(){
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getDataPresident())
        listPresident.adapter = adapter
    }

    private fun loadListBaseAdapter(context: Context) {
        list.addAll(PresidentsData.listDataPresident)
    
        val baseAdapter = ListViewPresidentAdapter(context, list)
        listPresident.adapter = baseAdapter
    }
        fun getDataPresident(): Array<String> {
            val president = arrayOf(
                "Soekarno",
                "Soeharto",
                "B.J. Habibie",
                "Abdurrahman Wahid",
                "Megawati Soekarnoputri",
                "Susilo Bambang Yudhoyono",
                "Joko Widodo",
                "Kim Jong Un",
                "Donald Trump",
                "Recep Tayyip Erdoğan"
            )
            return president
        }
    }
